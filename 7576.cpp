#include<stdio.h>
#include<queue>
using namespace std;

typedef struct Coor {
    int x, y;
} Coor;

Coor createCoor(int x, int y) {
    Coor coor;
    coor.x = x;
    coor.y = y;
    return coor;
}

queue<Coor> q;

int M, N, arr[1000][1000];

// 2 <= M, N <= 1000
// arr[m][n]
void userInput() {
    int m, n;
    scanf("%d %d", &M, &N);
    
    
    for(n = 0; n < N; n++) {
        for(m = 0; m < M; m++) {
            scanf("%d", &arr[m][n]);

            if(arr[m][n] == 1) q.push(createCoor(m, n));
        }
    }
}

int makeGrown(Coor coor) {
    if((0 <= coor.x && coor.x < M) && (0 <= coor.y && coor.y < N)) {
        if(arr[coor.x][coor.y] == 0) {
            arr[coor.x][coor.y] = 1;
            return 1;
        }
        else return 0;
    }
    else return 0;
}

int bfs() {
    int m, n;
    int i, size;
    int cnt = 0;
    int verNum = 0;
    Coor dir;
    while(!q.empty()) {
        size = q.size();
        for(i = 0; i < size; i++) {
            dir = createCoor(q.front().x - 1, q.front().y);
            if(makeGrown(dir)) q.push(dir);
            dir = createCoor(q.front().x, q.front().y - 1);
            if(makeGrown(dir)) q.push(dir);
            dir = createCoor(q.front().x + 1, q.front().y);
            if(makeGrown(dir)) q.push(dir);
            dir = createCoor(q.front().x, q.front().y + 1);
            if(makeGrown(dir)) q.push(dir);
            q.pop();
        }
        cnt++;
    }
    
    for(n = 0; n < N; n++) {
        for(m = 0; m < M; m++) {        
            if(arr[m][n] == 0) return -1;
        }
    }
    return cnt - 1;
}

int main(int argc, char const *argv[])
{    
    userInput();
    printf("%d\n", bfs());
    return 0;
}

#include<stdio.h>
#include<queue>
using namespace std;

typedef struct Coor {
    int x, y, z;
} Coor;

Coor createCoor(int x, int y, int z) {
    Coor coor;
    coor.x = x;
    coor.y = y;
    coor.z = z;
    return coor;
}

queue<Coor> q;

int M, N, H, arr[100][100][100];

// 2 <= M, N, H <= 100
// arr[m][n][h]
void userInput() {
    int m, n, h;
    scanf("%d %d %d", &M, &N, &H);
    
    for(h = 0; h < H; h++) {
        for(n = 0; n < N; n++) {
            for(m = 0; m < M; m++) {        
                scanf("%d", &arr[m][n][h]);
                if(arr[m][n][h] == 1) q.push(createCoor(m, n, h));
            }
        }
    }
}

int makeGrown(Coor coor) {
    if(
        (0 <= coor.x && coor.x < M) && 
        (0 <= coor.y && coor.y < N) &&
        (0 <= coor.z && coor.z < H)
    ) {
        if(arr[coor.x][coor.y][coor.z] == 0) {
            arr[coor.x][coor.y][coor.z] = 1;
            return 1;
        }
        else return 0;
    }
    else return 0;
}

int bfs() {
    int m, n, h;
    int i, size;
    int cnt = 0;
    int verNum = 0;
    Coor dir;
    while(!q.empty()) {
        size = q.size();
        for(i = 0; i < size; i++) {
            dir = createCoor(q.front().x - 1, q.front().y, q.front().z);
            if(makeGrown(dir)) q.push(dir);
            dir = createCoor(q.front().x, q.front().y - 1, q.front().z);
            if(makeGrown(dir)) q.push(dir);
            dir = createCoor(q.front().x + 1, q.front().y, q.front().z);
            if(makeGrown(dir)) q.push(dir);
            dir = createCoor(q.front().x, q.front().y + 1, q.front().z);
            if(makeGrown(dir)) q.push(dir);
            dir = createCoor(q.front().x, q.front().y, q.front().z - 1);
            if(makeGrown(dir)) q.push(dir);
            dir = createCoor(q.front().x, q.front().y, q.front().z + 1);
            if(makeGrown(dir)) q.push(dir);
            
            q.pop();
        }
        cnt++;
    }
    
    for(h = 0; h < H; h++) {
        for(n = 0; n < N; n++) {
            for(m = 0; m < M; m++) {        
                if(arr[m][n][h] == 0) return -1;
            }
        }
    }
    return cnt - 1;
}

int main(int argc, char const *argv[])
{    
    userInput();
    printf("%d\n", bfs());
    
    return 0;
}
